﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Koperasi
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var context = new KoperasiEntities();
                var siswa = new Siswa
                {
                    Nis = "01",
                    Nama = "Hendrawan",
                    Kelamin = true,
                    Alamat = "Brebes jawa tengah",
                    TempatLahir = "Kebumen",
                    TanggalLahir = DateTime.Parse("1983/04/18")
                };
                context.Siswa.Add(siswa);
                context.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
